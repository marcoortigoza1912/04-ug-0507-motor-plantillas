//Archivo que contiene las rutas del proyecto
const express = require("express");
const router = express.Router();
const db = require("../db/data")
const dbsqlite = require("../db/conexion");
const { getALL} = require ("../db/conexion");

require('dotenv').config({ path: '.env' });

router.get("/", async (request, response) => {
    // Obtener todos los integrantes desde la base de datos
    const integrantes = await getALL("SELECT * FROM integrantes");
    console.log(integrantes);
    // Definir la matrícula del home
    const matriculaDeHome = "Home";
    // Obtener los datos del home desde la base de datos
    const Home = await getALL(`SELECT * FROM home `);
    console.log("DatosDeHome", Home);
    // Renderizar la página index 
    response.render("index", {
        inicio: Home,
        integrantes: integrantes,
        //se trae los datos para el footer desde .env
        ENLACE: process.env.ENLACE,
        NOMBRE: process.env.NOMBRE,
        APELLIDO: process.env.APELLIDO,
        MATERIA: process.env.MATERIA
    });
});

/*router.get("/",  (request, response) => {
   
    response.render("index", {
        home: db.home[0],
        integrantes: db.integrantes,
        //se trae los datos para el footer
        ENLACE: process.env.ENLACE,
        NOMBRE: process.env.NOMBRE,
        APELLIDO: process.env.APELLIDO,
        MATERIA: process.env.MATERIA

    });
   
});*/
router.get("/paginas/integrantes/:matricula", async (request, response) => {
    const matricula = request.params.matricula;                                     
    const integrante = await getALL(`SELECT * FROM integrantes`);
    // se asocia la matricula del url con la matricula de la tabla media
    const datos = await getALL(`SELECT * FROM media WHERE matricula = '${matricula}'`);
    if (integrante.length === 0) {
        response.status(404).render("error");
    } else {
        response.render("integrante", {
            
            data: datos,
            integrantes: integrante,    
            ENLACE: process.env.ENLACE,
            NOMBRE: process.env.NOMBRE,
            APELLIDO: process.env.APELLIDO,
            MATERIA: process.env.MATERIA
        });
    }
});


/*router.get("/:matricula", (request, response, next) => {
    const matricula = request.params.matricula;

    // Buscar el integrante por la matrícula
    const integranteFilter = db.integrantes.find(integrante => integrante.matricula === matricula);

    // Filtrar los medios por la matrícula
    const medios = db.media.filter(media => media.matricula === matricula);

    // Verificar si se encontró el integrante o los medios
    if (!integranteFilter || medios.length === 0) {
        //renderizar la pagina "error" si el integrante es incorrecto
        response.render("error");
    }else{
        // Renderizar la página 'integrante' con los datos filtrados
    response.render('integrante', {
        integrante: integranteFilter,
        media: medios,
        tipomedia: db.tipomedia,
        integrantes: db.integrantes
    }); 
    }

    
});*/

/******************************************** */
router.get("/paginas/word_cloud.html", async(request, response) => {
    const integrante = await getALL(`SELECT * FROM integrantes`);
    response.render("word_cloud",{
        integrantes: integrante,
        ENLACE: process.env.ENLACE,
        NOMBRE: process.env.NOMBRE,
        APELLIDO: process.env.APELLIDO,
        MATERIA: process.env.MATERIA
    })
    
});
router.get("/paginas/curso.html", async(request, response) => {
    const integrante = await getALL(`SELECT * FROM integrantes`);
    response.render("curso",{
        integrantes: integrante,
        ENLACE: process.env.ENLACE,
        NOMBRE: process.env.NOMBRE,
        APELLIDO: process.env.APELLIDO,
        MATERIA: process.env.MATERIA
    });
    
});

module.exports = router;
const express = require('express');
const router = express.Router();
const { getALL, db} = require ("../db/conexion");
const fs = require('fs');
const multer = require('multer');
const upload = multer({ dest: "./public/assets/images"});
const fileUpload = upload.single("url");//nombre de columna imagen
//const db = require("../db/data")
const sqlite3 = require('sqlite3');
const { error } = require('console');



router.get('/', (req, res) =>{
    res.render("admin/index")
});
//LISTAR INTEGRANTES
router.get('/integrantes/listar', async(req, res) =>{      
                                
    const integrante = await getALL(`SELECT * FROM integrantes where borralog ='FALSE' order by orden asc`);
    if (integrante.length === 0) {
        response.status(404).render("error");
    } else {
        res.render("admin/integrantes/index",{
            integrantes: integrante,
        });
    }
});
//Render Formulario Integrantes
router.get('/integrantes/crear', async(req, res) =>{
    const mensaje = req.query.mensaje;
    res.render('admin/integrantes/crearFormulario', {
        mensaje: mensaje
    });
});
//Render Formulario Tipo - Media
router.get('/tipo_media/crear', async(req, res) =>{
    const mensaje = req.query.mensaje;
    res.render('admin/tipo_media/formularioTipoMedia', {
        mensaje: mensaje
    });
});
//Render Formulario Media
router.get('/media/crear', async(req, res) =>{
    const mensaje = req.query.mensaje;
    res.render('admin/media/formularioMedia', {
        mensaje: mensaje
    });
});

//LISTAR MEDIA
router.get('/media/listar', async(req,res)=> {
    const media = await getALL(`SELECT * FROM media where borralog ='FALSE' order by orden asc`);
    if (media.length === 0) {
        response.status(404).render("error");
    } else {
        res.render("admin/media/index",{
            media: media,
        });
    }
});
//LISTAR TIPO-MEDIA 
router.get('/tipo_media/listar', async(req,res)=> {
    const tipomedia = await getALL(`SELECT * FROM tipomedia where borralog ='FALSE' order by orden asc`);
    if (tipomedia.length === 0) {
        response.status(404).render("error");
    } else {
        res.render("admin/tipo_media/index",{
            tipomedia: tipomedia,
        });
    }
});
//Crear Integrante
router.post('/integrantes/create', (req, res) =>{
    const { matricula, nombre, apellido, borralog } = req.body;
    if (!req.body.matricula || req.body.matricula.trim() === ''
        || !req.body.nombre || req.body.nombre.trim() === ''
        || !req.body.apellido || req.body.apellido.trim() === ''
        || !req.body.borralog || req.body.borralog.trim() === '')
        return res.redirect(`/admin/integrantes/crear?mensaje="Todos los campos son obligatorios. Favor verificar"&matricula=${matricula}&nombre=${nombre}&apellido${apellido}&borralog=${borralog}`
        );
    console.log("contenido del formulario", req.body);
    
    db.get("SELECT MAX(orden) as orden FROM integrantes", [], (err, row) => {
        if (err) {
            console.error("Error al obtener el máximo orden:", err);
            return res.sendStatus(500);
        }
            const nuevoOrden = (row.orden || 0)+1;
            db.run(
                "INSERT INTO integrantes (matricula, nombre, apellido, borralog, orden) VALUES (?, ?, ?, ?, ?)",
                [matricula, nombre, apellido, borralog, nuevoOrden],
                (err) => {
                    if (err) {
                        console.error("Error al insertar en la base de datos:", err);
                        return res.sendStatus(500);
                    }
                    res.redirect("/admin/integrantes/listar?success=${encodeURIComponent('¡Registro insertado correctamente!')}");
                }
            );    
    });
});
//crear tipo - Media
router.post('/tipo_media/create', (req,res) => {
    const {nombre, borralog} = req.body;
    if (!req.body.nombre || req.body.nombre.trim() === ''
    || !req.body.borralog || req.body.borralog.trim() === '')
    return res.redirect(`/admin/tipo_media/crear?mensaje="Todos los campos son obligatorios. Favor verificar"&nombre=${nombre}&borralog=${borralog}`
   
    );

    console.log("contenido del formulario Tipo Media ", req.body );
    
    db.get("SELECT MAX(orden) as orden from tipomedia", [], (err,row) => {
            
            if(err){
                console.error("Error al insertar en la base de datos:",err)
                return res.sendStatus(500);
            }
            const nuevoOrden = (row.orden || 0)+1;

            db.run(
                "INSERT INTO tipomedia (id, nombre, borralog, orden) VALUES (?, ?, ?, ?)",
                [null,nombre, borralog, nuevoOrden],
                (err) => {
                    if (err) {
                        console.error("Error al insertar en la base de datos:", err);
                        return res.sendStatus(500);
                    }
                    res.redirect("/admin/tipo_media/listar?success=${encodeURIComponent('¡Registro insertado correctamente!')}");
                }
            );
    });   

});
    
//crear media
router.post('/media/create',  upload.single('archivo'), (req, res) => {
    if (!req.body.matricula || req.body.matricula.trim() === ''
        || !req.body.nombre || req.body.nombre.trim() === ''
        || !req.body.titulo || req.body.titulo.trim() === ''
        || !req.body.borralog || req.body.borralog.trim() === '')
        return res.redirect(`/admin/media/crear?mensaje="Todos los campos son obligatorios. Favor verificar"`
    );


    console.log("Contenido del formulario Media ", req.body );

    var tmp_path = req.file.path;
    console.log("tmp_path", tmp_path);

    var destino = "./public/assets/images/" + req.file.originalname;
    console.log("destino", destino);

    if (!req.file) {
        console.error("No se proporcionó ningún archivo");
        return res.sendStatus(400); // Bad Request
    }

    const {nombre, url, titulo, borralog, matricula} = req.body;
    db.get("SELECT orden FROM integrantes WHERE matricula = ?", [matricula], (err, row) => {
        if(err){
            console.error("Error al ejecutar la consulta:", err);
            return res.sendStatus(500);
        }
        //const nuevoOrden = (row.orden = matricula);
        const nuevoOrden = row ? row.orden : 0;
        db.run(
            "INSERT INTO media (id, nombre, src, url, titulo, borralog, matricula, orden) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
            [null, nombre, null, url+ req.file.originalname, titulo, borralog, matricula, nuevoOrden], 
            (err) => {
                if (err) {
                    console.error("Error al insertar en la base de datos:", err);
                    return res.sendStatus(500);
                }
                res.redirect("/admin/media/listar?success=${encodeURIComponent('¡Registro insertado correctamente!')}");
            }
        );
    });
});

module.exports = router;





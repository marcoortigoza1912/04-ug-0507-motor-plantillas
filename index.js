const express = require('express');

const hbs = require("hbs");
const app = express();
require("dotenv").config({ path: '.env' });
const bodyParser = require('body-parser');

//Import archivo de Rutas
app.use(bodyParser.urlencoded({ extended: false}));
const router = require("./routes/public");
const routerAdmin = require('./routes/admin');
app.use("/", router);
app.use("/admin", routerAdmin);



//conf para app express
app.use(express.static('public'));
//app.use("/admin", express.static('admin'));
app.set('view engine', 'hbs');
app.set('views', __dirname + '/views');

hbs.registerPartials(__dirname + "/views/partials");

const puerto = process.env.PORT || 3000;
    

app.listen(puerto, () => {
    console.log("El servidor se está ejecutando en en el puerto " + puerto );
});








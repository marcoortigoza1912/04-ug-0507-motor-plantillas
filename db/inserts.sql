--INSERTS TABLA INTEGRANTES
INSERT INTO integrantes (nombre, apellido, matricula, borralog) VALUES 
('Marco', 'Ortigoza', 'UG0507',FALSE),
('Elvio', 'Martinez', 'Y12858',FALSE),
('Alexis', 'Duarte', 'Y17825',FALSE),
('Gabriel', 'Garcete', 'Y23865',FALSE),
('Lennys', 'Cantero', 'Y26426',FALSE);
SELECT* FROM integrantes

--INSERTS TABLA TIPOMEDIA
INSERT INTO tipomedia (nombre, borralog) VALUES 
('Imagen',FALSE),
('Youtube',FALSE),
('Dibujo',FALSE);
SELECT * FROM tipomedia

--inserts tabla media
INSERT INTO media (nombre, src, url, titulo, matricula, borralog) VALUES ('Youtube', NULL, 'https://www.youtube.com/embed/b8-tXG8KrWs?si=qBWFL28iK9JTU3JZ', 'Video favorito de youtube', 'Y26426',FALSE);
INSERT INTO media (nombre, src, url, titulo, matricula, borralog) VALUES ('imagen', NULL, '/assets/images/imagen-56.jpeg', 'Imagen representativa', 'Y26426',FALSE);
INSERT INTO media (nombre, src, url, titulo, matricula, borralog) VALUES ('dibujo', NULL, '../../assets/images/imagen-57.png', 'Dibujo en Paint', 'Y26426',FALSE);

INSERT INTO media (nombre, src, url, titulo, matricula, borralog) VALUES ('Youtube', NULL, 'https://www.youtube.com/embed/RW75cGvO5xY', 'Video favorito de youtube', 'UG0507',FALSE);
INSERT INTO media (nombre, src, url, titulo, matricula, borralog) VALUES ('Imagen', NULL, '/assets/images/saturno.jpg', 'Imagen representativa', 'UG0507',FALSE);
INSERT INTO media (nombre, src, url, titulo, matricula, borralog) VALUES ('Dibujo', NULL, '../../assets/images/TERERE.png', 'Dibujo en paint', 'UG0507',FALSE);

INSERT INTO media (nombre, src, url, titulo, matricula, borralog) VALUES ('Youtube', NULL, 'https://www.youtube.com/embed/VhoHnKuf-HI', 'Video favorito de youtube', 'Y12858',FALSE);
INSERT INTO media (nombre, src, url, titulo, matricula, borralog) VALUES ('Imagen', NULL, '../../assets/images/melissa.jpg', 'Imagen representativa', 'Y12858',FALSE);
INSERT INTO media (nombre, src, url, titulo, matricula, borralog) VALUES ('Dibujo', NULL, '../../assets/images/dibujo-Elvio.png', 'Dibujo en paint', 'Y12858',FALSE);

INSERT INTO media (nombre, src, url, titulo, matricula, borralog) VALUES ('Youtube', NULL, 'https://www.youtube.com/embed/U1ivmi3_IeI?si=9QjM1bJzS3uUpMYk', 'Video favorito de youtube', 'Y17825',FALSE);
INSERT INTO media (nombre, src, url, titulo, matricula, borralog) VALUES ('Imagen', NULL, '../../assets/images/imagen-personalidad.jpg', 'Imagen representativa', 'Y17825',FALSE);
INSERT INTO media (nombre, src, url, titulo, matricula, borralog) VALUES ('Dibujo', NULL, '../../assets/images/dibujo-sistema-solar.png', 'Dibujo en paint', 'Y17825',FALSE);

INSERT INTO media (nombre, src, url, titulo, matricula, borralog) VALUES ('Youtube', NULL, 'https://www.youtube.com/embed/B4LvDiIi128?rel=0', 'Video favorito de youtube', 'Y23865',FALSE);
INSERT INTO media (nombre, src, url, titulo, matricula, borralog) VALUES ('Imagen', NULL, '../../assets/images/messi_pou.jpeg', 'Imagen representativa', 'Y23865',FALSE);
INSERT INTO media (nombre, src, url, titulo, matricula, borralog) VALUES ('Dibujo', NULL, '../../assets/images/paint_garcete.jpg', 'Dibujo en paint', 'Y23865',FALSE);

SELECT * from media


--INSERT TABLA HOME
INSERT INTO home (nombre, titulo, src, alt) VALUES 
('FSOCIETY', 'Bienvenidos al grupo', '/assets/images/logo.jpeg', 'Grupo FSOCIETY');
